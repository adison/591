#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

import re
import sys
from datetime import datetime
import time

import sqlite3
import json
import requests
import urllib.request
from slackclient import SlackClient
from bs4 import BeautifulSoup
from os.path import expanduser

# for 591.com

class MyException(Exception):
    pass

class AppURLopener(urllib.request.FancyURLopener):
    version = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"

def main():
    opener = AppURLopener()
    conn = sqlite3.connect( expanduser("~") + '/Dropbox/appsync/591.sqlite')
    cur = conn.cursor()

    # module
    module = "search"
    # action
    action = "rslist"
    # actionType
    actionType = "1" 
    # searchtyp
    searchtype = "1"
    # kind, 1: 租
    kind = "1" 
    # rent price, 0: 不限, 1: ~5k, 2: 5-10k, 3: 10-15k, 4: 15-20k, 5: 20-40k, 6: 40k+
    # 逗點表示前後為價格
    rentprice = "15000,26000"

    # region, 1 = 台北, 3 = 新北
    region = ["1", "3"]
    # section, 區域
    # 26: 三重, 47 蘆洲
    # 北市全區, 板橋區中和區新店區永和區
    section = ["", "34,37,38,43"]
    url591s = []
    for reg, sec in zip(region, section):
        url591s.append( "https://rent.591.com.tw/index.php?module={}&action={}&is_new_list=1&type={}&searchtype={}&region={}{}&kind={}&rentprice={}".format(module
            , action
            , actionType
            , searchtype
            , reg
            , ("&section={}".format(sec) if not sec == "" else "")
            , kind
            , rentprice))

    for url591 in url591s:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
            , 'Origin:': url591
            , 'Connection': 'keep-alive'
            , 'Cache-Control': 'max-age=0'
            , 'Upgrade-Insecure-Requests': 1
            , 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
            , 'Referer': url591
            , 'Accept-Encoding': 'gzip, deflate, sdch'
            , 'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4'
        }

        r = requests.get(url591, headers)
        # rawString = bytes(r.text, encoding = "utf8").decode('unicode_escape')
        rawJson = json.loads(r.text)
        rentListHtml = re.sub( r'\\/', '/', rawJson['main'])
        rentListHtml = re.sub(r"\\r\\n", "", rentListHtml)
        rentListHtml = re.sub(r"\\\"", "", rentListHtml)
        # logstring(rentListHtml)
        rentList = BeautifulSoup(rentListHtml, "lxml")

        checkDB(cur)
        userMSg = ""
        i = 0
        for tag in rentList.find_all("ul"):
            if not tag.get("class") or not tag.get("class")[0].startswith("shInfo"):
                continue
            i += 1
            title = ""
            link = ""
            mainImg = ""
            address = ""
            description = ""
            price = ""
            for item in tag.find_all("li"):
                itemClass = item["class"][0]
                if itemClass == "info":
                    for detailDiv in tag.find_all("div"):
                        classString = detailDiv["class"][0]
                        if classString == "left":
                            for imgbd in tag.find_all("a"):
                                if not imgbd.get("class") or not imgbd.get("class")[0] == "imgbd":
                                    continue
                                title = imgbd["title"]
                                link = "https://rent.591.com.tw/" + imgbd["href"]
                                for imgLink in imgbd.find_all("img"):
                                    mainImg = imgLink['src']
                                    break
                        elif classString == "right":
                            # for imgbd in tag.find_all("p"):
                            for para in detailDiv.find_all('p'):
                                if para.get('class'):
                                    continue
                                elif address == "":
                                    # address: 新北市-蘆洲區 保和街26巷
                                    address = para.get_text()
                                else:
                                    # description: 整層住家，2房1廳1衛，樓層：6/6
                                    description = para.get_text()
                elif itemClass == "area":
                    # area: 20坪
                    description += ", {}".format(re.sub(r"\s", "", item.get_text()))
                elif itemClass == "price":
                    for rentP in item.find_all("strong"):
                        if not rentP.get_text() == "":
                            price = re.sub(r"\s", "", rentP.get_text())
                # elif itemClass == "pattern":
                #     pass
            msg = insertData(cur, conn, title, link, mainImg, address, description, price)
            if msg:
                userMSg += msg

        print(str(i) + "筆資料 in " + url591)
        print(userMSg)
        if not userMSg == "":
            callUser(userMSg)
        logstring("\n591 data update completed: " + str(datetime.now()) + "\n\t" + url591)

    conn.commit()
    conn.close()
    sys.exit()

# logstring, 印出來同時也寫到文件中
def logstring(aString):
    file = open("591.log", "a+")
    file.write(aString + "\n")
    print(aString)

# call user
def callUser(aMsg):
    token = open("slack-token.cer", "r").read()
      # found at https://api.slack.com/web#authentication
    slack = SlackClient(token)
    # logstring(slack.api_call("api.test"))
    # logstring('there is new entry {}, {}'.format(title, link))
    slack.api_call(
            "chat.postMessage", channel="#rent", text=aMsg,
            username='adison', icon_emoji=':robot_face:'
    )

# insert data
def insertData(cur, conn, title, link, image, area, description, price):
    try:
        if '頂樓加蓋' in description:
            raise MyException("頂樓加蓋")
        if 'nophoto' in image:
            # 因為 591 可能沒有圖
            cur.execute('''INSERT INTO rentlist (title, link, img, area, description, rentprice) 
             VALUES ( ?, ?, ?, ?, ?, ? )''', ( title, link, image, area, description, price) )
        else:
            cur.execute('''INSERT INTO rentlist (title, link, area, description, rentprice) 
             VALUES ( ?, ?, ?, ?, ? )''', ( title, link, area, description, price) )
        conn.commit()
        logstring("insert.. \n\ttitle:{}, @ {}\n\t${}, {}\n\t{}".format(title, area, price, description, link))

        # 取得詳細資料，然後附加到說明中
        detailInfo = detailPageData(link)
        return "新物件\n*{}* {}, ${}\n> {}\n> {}\n{}\n```\n{}```\n".format(area, title, price, description, link, image, detailInfo)
    except sqlite3.IntegrityError:
        logstring('\t\tdata already: ' + title)
        pass
    except sqlite3.Error:
        logstring("sql error")
    except MyException as e:
        logstring("頂樓加蓋物件 \n\ttitle:{}, @ {}\n\t${}, {}\n\t{}".format(title, area, price, description, link))
    except Exception as e:
        logstring("Error data \n\ttitle:{}, @ {}\n\t${}, {}\n\t{}".format(title, area, price, description, link))
        logstring("DB error:" + str(e) + "\n")
        raise e

def detailPageData(link):
    headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
            , 'Origin:': link
            , 'Connection': 'keep-alive'
            , 'Cache-Control': 'max-age=0'
            , 'Upgrade-Insecure-Requests': 1
            , 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
            , 'Referer': link
            , 'Accept-Encoding': 'gzip, deflate, sdch'
            , 'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4'
        }
    r = requests.get(link, headers)
    rentList = BeautifulSoup(r.text, "lxml")

    detailData = []
    # 地址
    for tag in rentList.find_all("span"):
        if tag.get("class") and tag.get("class")[0].startswith("addr"):
             # 新北市蘆洲區忠孝路
            detailData.append(tag.text)
    # 右側資料
    for tag in rentList.find_all("ul"):
        if tag.get("class") and tag.get("class")[0].startswith("attr"):
            detailData.append('\n'.join(item.text for item in tag.find_all('li')))
    # 屋況說明
    for tag in rentList.find_all("div"):
        if tag.get("class") and tag.get("class")[0].startswith("houseIntro"):
            detailData.append(re.sub(r"\n\n", "\n", tag.text))
    # print(detailData)
    return '\n-----------------\n'.join(desc for desc in detailData)

# check DB
def checkDB(cur):
    # conn = sqlite3.connect('591.sqlite')
    # cur = conn.cursor()

    # Make some fresh tables using executescript()
    cur.executescript('''
    CREATE TABLE IF NOT EXISTS rentlist (
        id     INTEGER PRIMARY KEY ON CONFLICT ROLLBACK AUTOINCREMENT NOT NULL,
        title  TEXT,
        link   TEXT    UNIQUE ON CONFLICT FAIL,
        img    TEXT,
        area   TEXT,
        description TEXT,
        rentprice   TEXT
    );
    ''')
    # conn.close()

# main
if __name__ == "__main__":
    main()