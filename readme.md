
#環境
此程式使用 python3
請安裝 requests, SlackClient, BeautifulSoup
安裝這三個之後，其他套件應該也安裝完了
由於 BeautifulSoup 使用了 lxml 解析引擎，所以可能需要 c 環境
MAC 上建議先安裝 homebrew, 然後安裝 python3 
接著就可以用 pip3 install xxx 指令安裝

pip3 install lxml
pip3 install request
pip3 install beautifulsoup4


#slack
請申請一個 slack，然後取得 API token
接著建立一個名稱為 slack-token.cer 的檔案，並將 API token 存進去
token 長度應該有50以上


#launchctl

複製檔案過去，可以考慮改名
launchctl unload ~/Library/LaunchAgents/com.zerowidth.launched.591check.plist
launchctl load -w ~/Library/LaunchAgents/com.zerowidth.launched.591check.plist


設定執行環境，不然會有編碼錯誤
launchctl setenv zh_TW.UTF-8